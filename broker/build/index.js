"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
const winston = require("winston");
const moment = require("moment");
const { extend } = moleculer_1.Logger;
const core_scheduler_1 = require("@pcmrbotjs/core-scheduler");
const broker = new moleculer_1.ServiceBroker({
    logFormatter: "short",
    transporter: "TCP",
    namespace: "pcmrbotjsv2",
    logger: bindings => extend(winston.createLogger({
        format: winston.format.combine(winston.format.label({ label: bindings.mod }), winston.format.timestamp(), winston.format.colorize(), winston.format.printf(({ level, message, label, timestamp }) => {
            return `[${moment(timestamp).format("HH:mm:ss.SSS")}] ${level} ${label.toUpperCase()}: ${message}`;
        })),
        transports: [
            new winston.transports.Console(),
        ]
    }))
});
core_scheduler_1.default(broker);
broker.start();
