const { ServiceBroker } = require("moleculer");

const broker = new ServiceBroker({
	logFormatter: "simple",
	transporter: "TCP",
	namespace: "pcmrbotjsv2"
});

broker.createService({
	name: "test",
	settings: {
		test: 1,
		othertest: "whatever"
	},
	events: {
		"$node.connected"(payload, sender, event) {
			console.log(payload, event);
		},
		"$node.updated"(payload, sender, event) {
			console.log(payload, event);
		},
		"$node.disconnected"(payload, sender, event) {
			console.log(payload, event);
		},
	}
});

broker.createService({
	name: "test",
	settings: {
		test: 2,
		othertest: "uh"
	}
});

broker.repl();
broker.start();
